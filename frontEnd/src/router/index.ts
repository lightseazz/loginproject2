import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '@/views/LoginView.vue'
import checkExistedSessionApi from '@/api/checkExistedSession.api'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: LoginView
    },
    {
      path: '/homePage',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    }
  ]
})
router.beforeEach(async (to, from) => {
  const sessionId = localStorage.getItem('sessionId');
  const response = await checkExistedSessionApi(sessionId ?? "");
  if(!response.isExisted && to.name !== 'login'){
    return { name: 'login' }
  }
  if(response.isExisted && to.name === 'login'){
    return { name: 'home' }
  }
})


export default router
