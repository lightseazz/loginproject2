export default async (email: string, password: string) => {
    try {
      const response = await fetch("http://localhost:80" + "/api/login", {
        method: "POST",
        headers: {
          "content-type": "application/json",
          accept: "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
        }),
      });
      return response.json();
    } catch (error) {
      return error;
    }
  }; 