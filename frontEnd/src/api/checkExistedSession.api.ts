export default async (sessionId: string) => {
    try {
      const response = await fetch("http://localhost:80" + "/api/checkExistedSession", {
        method: "POST",
        headers: {
          "content-type": "application/json",
          accept: "application/json",
        },
        body: JSON.stringify({
          sessionId: sessionId,
        }),
      });
      return response.json();
    } catch (error) {
      return error;
    }
  }; 