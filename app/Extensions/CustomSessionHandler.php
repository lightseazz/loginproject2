<?php
 
namespace App\Extensions;
use Illuminate\Session\DatabaseSessionHandler;
 
class CustomSessionHandler extends DatabaseSessionHandler 
{
    protected function getDefaultPayload($data)
    {
        $payload = [
            'payload' => base64_encode($data),
            'last_activity' => $this->currentTime(),
            'sites' => "batman",
        ];

        if (! $this->container) {
            return $payload;
        }

        return tap($payload, function (&$payload) {
            $this->addUserInformation($payload)
                 ->addRequestInformation($payload);
        });
    }
}



