<?php
 
namespace App\Providers;
 
use App\Extensions\CustomSessionHandler;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
 
class SessionServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        // ...
    }
 
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Session::extend('custom', function (Application $app) {
            $config = $app->make('config');
            $connection = $config->get('session.connection');
            $connection = $app->make('db')->connection($connection);
            // Return an implementation of SessionHandlerInterface...
            return new CustomSessionHandler(
                $connection,
                $config->get('session.table'),
                $config->get('session.lifetime'),
                $app
            );
        });
    }
}