<?php

namespace App\Http\Controllers;

use App\Models\Session;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function checkExistedSession(Request $request)
    {
        $sessionId = $request->sessionId;
        $session = Session::where('id', $sessionId)->first();
        if ($session && $session->user_id) {
            return response()->json(['detail' => 'redirect user to homePage.', 'isExisted' => true], 200);
        }
        return response()->json(['detail' => 'redirect user to loginPage', 'isExisted' => false], 200);

    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required'],
            'password' => ['required'],
        ]);

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json(['detail' => 'The provided credentials are incorrect.', 'ok' => false], 401);
        }
        $userId = $user->getKey();

        if (!$this->checkSessionValid($userId)) {
            return response()->json(['detail' => 'another User is logged in', 'ok' => false], 401);
        }

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return response()->json(
                [
                    'detail' => 'login successfully',
                    'ok' => true,
                    'sessionId' => $request->session()->getId()
                ]
                ,
                200
            );
        }

        return response()->json(['detail' => 'The provided credentials are incorrect.', 'ok' => false], 401);
    }

    private function checkSessionValid($userId)
    {
        $session = Session::where('user_id', $userId)->first();
        if (!$session) {
            return true;
        }
      
        return false;

    }

}

