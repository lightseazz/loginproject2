<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class RegisterController extends Controller
{
 
    public function register(Request $request)
    {
        // $request->validate([
        //     'name' => 'required|max:255',
        //     'email' => 'require|email',
        //     'password' => 'require|min:2'
        // ]);


        $existingUser = User::where('email', $request->email)->first();

        if($existingUser){
            return response()->json(['error' => 'User with this email already exist.'], 409);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json($user, 201);

    }
}
